#include<iostream>
using namespace std;

class canvas;


class point{
	public:
		point();
		point(int x, int y);
		point(double x, double y);
		double get_x() const;
		double get_y() const;
		void set_x(double in_x);
		void set_y(double in_y);
		point operator+(const point &right);
		point operator+(double right);
		point operator-(const point &right);
		point operator-(double right);
		point &operator=(const point &right);
		friend ostream &operator<<(ostream &left, const point &right); /*overloaded << operator so there is no need for a print 
		function for this class*/
	private:
		double x,y;
};

class color{
	public:
	    color();
		color(unsigned char R, unsigned char G, unsigned char B);
		friend canvas;
		unsigned char getR(void) const;
		unsigned char getG(void) const;
		unsigned char getB(void) const;
		void setR(const unsigned char r);
		void setG(const unsigned char g);
		void setB(const unsigned char b);
        color &operator=(const color &right);
        friend ostream &operator<<(ostream &left, const color &right);
	private:
		unsigned int Red, Green, Blue;
};

class canvas{
	private:
		int rows, columns; // r = Y c = X
		color **buffer; /* each buffer element/each pixel has a specific color  */
	public:
		canvas();
		canvas(int x, int y);
		~canvas();
		int get_rows() const;
		int get_columns() const;
		void paint();
		void color_point(const point &p1, const color &c1);
		void set_size(int x, int y);
		void clear();
};