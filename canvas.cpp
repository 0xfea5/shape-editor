#include<iostream>
#include"canvas.h"
using namespace std;


double point::get_x() const{
	return x;
}
double point::get_y() const{
	return y;
}
void point::set_x(double in_x){
	x = in_x;
}
void point::set_y(double in_y){
	y = in_y;
}

ostream &operator<<(ostream &left, const point &right){
	left<<"("<<right.x<<","<<right.y<<")";
	return left;
}	

point::point(){
	x = 0.0;
	y = 0.0;
} 

point::point(double in_x, double in_y){
	x = in_x;
	y = in_y;
}

point point::operator+(const point &right){
	point result;

	result.x = x + right.x;
	result.y = y + right.y;
	return result;
}

point point::operator+(double right){
	point result;

	result.x = x + right;
	result.y = y + right;
	return result;
}

point point::operator-(const point &right){
	point result;

	result.x = x - right.x;
	result.y = y - right.y;
	return result;
}

point point::operator-(double right){
	point result;

	result.x = x - right;
	result.y = y - right;
	return result;
}


point &point::operator=(const point &right){
	if(this == &right){
		return *this;
	}
	x = right.x;
	y = right.y;
}


color::color(){
    Red = 0;
	Green = 0;
	Blue = 0;
}

color::color(unsigned char R, unsigned char G, unsigned char B){
	Red = R;
	Green = G;
	Blue = B;
}

unsigned char color::getR(void) const{
	return Red;
}

unsigned char color::getG(void) const{
	return Green;
}

unsigned char color::getB(void) const{
	return Blue;
}

color &color::operator=(const color &right){
    if(this == &right){
        return *this;
    }
    Red = right.Red;
    Green = right.Green;
    Blue = right.Blue;
}

void color::setR(const unsigned char r){
	Red = r;
}

void color::setG(const unsigned char g){
	Green = g;
}

void color::setB(const unsigned char b){
	Blue = b;
}

ostream &operator<<(ostream &left, const color &right){
    unsigned char r = right.Red, g = right.Green, b = right.Blue;
	left<<"["<<(int) r<<", "<<(int) g<<", "<<(int) b<<"]";
	return left;
}

canvas::canvas(){
	rows = 0;
	columns = 0;
	buffer = NULL;
}

canvas::canvas(int x, int y){
	if((x > 0)&&(y > 0)){
		rows = x;
		columns = y;
	 	buffer = new color* [x];
	 	if(buffer != NULL){
	     	int i;
	    	for( i = 0; i < x; i++){
	    		buffer[i] = new color [y];
	    	}
	    	int j;
	       	for(i = 0; i < rows; i++){
	    	    for(j = 0; j < columns; j++){
	    	        buffer[i][j].Red = 0;
	    	        buffer[i][j].Green = 0;
	    	        buffer[i][j].Blue = 0;
	     	    }
	    	}
	    } else{
	        cout<<"Error allocating memory!"<<endl;
	    }
  	} else {
  		cout<<"Invalid canvas size."<<endl;
  	}
}

canvas::~canvas(){
	if(buffer != NULL){
	    int i;
		for(i = 0; i < rows; i++){
	 		delete [] buffer[i];
		}
		delete buffer;
	}
}

void canvas::color_point(const point &p1, const color &c1){
	int i = p1.get_x(), j = p1.get_y();
	if((i < rows)&&( j < columns)&&(i >= 0)&&(j >= 0)){
		buffer[i][j] = c1;
	} else{
		cout<<"Point out of bounds."<<endl;
	}
}

void canvas::paint(){
	int i,j;
	for(i = 0; i < columns*10 + 3; i++){
		cout<<"_";
	}
	cout<<endl;
	for(i = rows - 1; i >= 0; i--){
		cout<<"| ";
		for(j = 0; j < columns - 1; j++){
			cout<<buffer[i][j]<<" ";
		}
		cout<<buffer[i][j]<<" |"<<endl;
	}
	for(i = 0; i < columns*10 + 3; i++){
		cout<<"_";
	}
	cout<<endl;
}

int canvas::get_rows() const{
    return rows;
}


int canvas::get_columns() const{
    return columns;
}

void canvas::clear(){
    int i,j;
   	for(i = 0; i < rows; i++){
	    for(j = 0; j < columns; j++){
	        buffer[i][j].Red = 0;
	        buffer[i][j].Green = 0;
	        buffer[i][j].Blue = 0;
 	    }
	}
}

void canvas::set_size(int x, int y){
	if((x > 0)&&(y > 0)){
		rows = x;
		columns = y;
		if(buffer != NULL){
	   		int i;
			for(i = 0; i < rows; i++){
		 		delete [] buffer[i];
			}
			delete buffer;
		}
	 	buffer = new color* [x];
	 	if(buffer != NULL){
	     	int i;
	    	for( i = 0; i < x; i++){
	    		buffer[i] = new color [y];
	    	}
	    	int j;
	       	for(i = 0; i < rows; i++){
	    	    for(j = 0; j < columns; j++){
	    	        buffer[i][j].Red = 0;
	    	        buffer[i][j].Green = 0;
	    	        buffer[i][j].Blue = 0;
	     	    }
	    	}
	    } else{
	        cout<<"Error allocating memory!"<<endl;
	    }
  	} else {
  		cout<<"Invalid canvas size."<<endl;
  	}
}