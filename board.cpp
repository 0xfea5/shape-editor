#include<iostream>
#include<cstdlib>
#include"rasterboard.h"
using namespace std;

/*temporary raster board*/

int main(){
	int choice;
	int temp_int;
	board b1(40);
	point p1;
	char tmp;
	cout<<"Menu:"<<endl;
	cout<<"1. Insert character to a specific point"<<endl;
	cout<<"2. Print board"<<endl;
	cout<<"3. Clear board"<<endl;
	cout<<"4. Exit"<<endl;
	cout<<"Choose action: ";
	while(1){
		cin>>choice;
		switch(choice){
			case 1:
				cout<<"Type x-axis: ";
				cin>>temp_int;
				p1.set_x(temp_int);
				cout<<"Type y-axis: ";
				cin>>temp_int;
				cout<<"Type your character: ";
				cin>>tmp;
				p1.set_y(temp_int);
				b1.insert_point(p1,tmp);
				break;
			case 2:
				cout<<b1;
				break;
			case 3:
				b1.clear();
				break;
			case 4:
			    cout<<"Goodbye everybody, I got to go! Gonna leave you all behind and face the truth!"<<endl;
				exit(0);
			default:
			    cout<<"Invalid choice. Please give me an integer from 1 to 4"<<endl;
        }
        cout<<"Choose action: ";
	}
	return 0;
}
