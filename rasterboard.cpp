#include<iostream>
#include<cstdlib>
#include"rasterboard.h"
using namespace std;

double point::get_x() const{
	return x;
}
double point::get_y() const{
	return y;
}
void point::set_x(double in_x){
	x = in_x;
}
void point::set_y(double in_y){
	y = in_y;
}

ostream &operator<<(ostream &left, const point &right){
	left<<"("<<right.x<<","<<right.y<<")";
	return left;
}	

point::point(){
	x = 0.0;
	y = 0.0;
} 

point::point(double in_x, double in_y){
	x = in_x;
	y = in_y;
}


board::board(int in_n){
	N = in_n;
	p = new char* [N];
	if(!p){
		cout<<"Error allocating memory!"<<endl;
		exit(0);
	}
	int i;
	for(i = 0; i < N; i++){
		p[i] = new char [N];
		if(!p[i]){
			cout<<"Error allocating memory!"<<endl;
			exit(0);
		}
	}
	int j;
	for(i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			p[i][j] = '.';
		}	
	}
}

board::~board(){
	int i;
	for(i = 0; i < N; i++){
		delete [] p[i];
	}
	delete p;
}

board::board(const board &ob){
	N = ob.N;
	int i,j;
	p = new char* [N];
	if(!p){
		cout<<"Error allocating memory!"<<endl;
		exit(0);
	}
	for(i = 0; i < N; i++){
		p[i] = new char [N];
		if(!p[i]){
			cout<<"Error allocating memory!"<<endl;
			exit(0);
		}
	}
	for(i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			p[i][j] = ob.p[i][j];
		}	
	}
}

board &board::operator=(const board &ob){
	if(p != NULL){
		int i;
		for(i = 0; i < N; i++){
			delete [] p[i];
		}
		delete p;
	}
	N = ob.N;
	int i,j;
	p = new char* [N];
	if(!p){
		cout<<"Error allocating memory!"<<endl;
		exit(0);
	}
	for(i = 0; i < N; i++){
		p[i] = new char [N];
		if(!p[i]){
			cout<<"Error allocating memory!"<<endl;
			exit(0);
		}
	}
	for(i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			p[i][j] = ob.p[i][j];
		}	
	}
}


ostream &operator<<(ostream &left, const board &right){
	int n = right.N;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			cout<<right.p[i][j];
		}
		cout<<endl;
	}
	return left;
}


void board::insert_point(point &ob, char in_c){
	int i = ob.get_x(), j = ob.get_y();
	if((i >= 0)&&(j >= 0)&&(i < N)&&(j < N)){
		p[i][j] = in_c;
	} else{
	    cout<<"Point given out of raster bounds."<<endl;
	}
}

void board::clear(){
	int i,j;
	for(i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			p[i][j] = '.';
		}	
	}
}