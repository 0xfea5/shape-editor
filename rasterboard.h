#include<iostream>
using namespace std;

class point{
	public:
		point();
		point(double x, double y);
		double get_x() const;
		double get_y() const;
		void set_x(double in_x);
		void set_y(double in_y);
		friend ostream &operator<<(ostream &left, const point &right);
	private:
		double x,y;
};


class board{
	public:
		board(int in_n);
		board(const board &ob);
		~board();
		board &operator=(const board &ob);
		void insert_point(point &ob, char in_c);
		void clear();
		friend ostream &operator<<(ostream &left, const board &right);
	private:
		int N;
		char **p;
};